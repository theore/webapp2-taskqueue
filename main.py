#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging

import webapp2
from google.appengine.api import taskqueue


class RunQueueHandler(webapp2.RequestHandler):
    def get(self):

        for j in range(10):
            message = "---- task #%02d" % j
            taskqueue.add(queue_name='mytask', url='/worker', params={'message': message})


class WorkerHandler(webapp2.RequestHandler):
    def post(self):
        logging.info(self.request.get('message'))


class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello world!')


app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/worker', WorkerHandler),
    ('/runtask', RunQueueHandler)
], debug=True)
